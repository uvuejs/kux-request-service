## 1.0.1（2025-01-10）
+ 修复控制台提示 `promise` 异常问题。
+ 新增 `handleResponseErrorWithRetry` 处理响应错误并重试方法。调用示例代码：
	+ 请求库调用演示
	
	```
	// 假设 `response` 是响应错误的结果
	if (response != null) {
		// 请求库实例设置的当前重试次数标识
		this.attempts++;
		/**
		 * this.retryRequest为请求库实例设置的存储当前重试请求
		 */
		// #ifndef APP-ANDROID
		this.retryRequest = (url, options) => this.request(url, options);
		// #endif
		// #ifdef APP-ANDROID
		this.retryRequest = UTSAndroid.getKotlinFunction(request) as RetryFunction<RequestConfig, RequestSuccess<Response>> | null;
		// #endif
		if (this.interceptorManager.get(this.interceptorId)?.responseInterceptorErrorWithRetry == null) {
			return await this.interceptorManager.handleResponseError(response);
		}
		if (this.attempts <= retryTimes && this.retryRequest != null) {
			const withRetryOptions = {
				response,
				retryOptions: {
					retryTimes: this.attempts,
					retry: this.retryRequest as RetryFunction<RequestConfig, RequestSuccess<Response>>
				} as RetryOptions<RequestConfig, RequestSuccess<Response>>
			} as ResponseErrorCallbackOptions<RequestConfig, RequestSuccess<Response>>
			return (await this.interceptorManager.handleResponseErrorWithRetry(withRetryOptions));
		}
	}
	```
	
	+ 页面使用演示

	```
	// 假设httpService是初始化过的请求库实例
	httpService.use({
		...其他拦截器方法
		// 请求重试拦截
		responseInterceptorErrorWithRetry: async (options): Promise<RequestSuccess<Response> | null> => {
			console.log('请求失败了可以重新尝试', options);
			return options.retryOptions.retry('https://test.api.fdproxy.cn/user/list', {} as RequestConfig);
		}
	} as RIInterceptor<RequestConfig, RequestSuccess<Response>>);
	```
	
	> **提示**
	> 
	> 请求重试拦截实例可以看文档最下面 `完整自定义请求拦截示例`
## 1.0.0（2025-01-06）
+ 初始版本发布。
