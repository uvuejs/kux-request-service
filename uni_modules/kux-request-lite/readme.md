# kux-request-lite
支持泛型的轻量级请求库，自己可以基于该请求库做自己的请求拦截器。

## 插件特色
+ 支持泛型
+ 轻量化
+ 支持微信小程序 (需编译器版本4.25及以上)

## 使用示例
```ts
import { request } from '@/uni_modules/kux-request-lite';
type Data = {
	id: number
	name: string
	age: number
}
type Response = {
	route?: string
	method?: string
	statusCode: number
	message: string
	data?: Data[]
	error?: string
}
	
const demo = async () => {
	const { data, error, response } = await request<Response>({
		url: 'https://test.api.fdproxy.cn/user/list'
	} as RequestOptions<Response>);
	
	if (error != null) {
		console.log(error);
		return;
	}
	
	console.log(response);
	
	if (data != null) {
		console.log(data.data);
	}
}
	
demo();
```

## request返回结果说明
### data
服务器接口返回的结果

### error
服务器接口请求错误的结果

### response
[uni.request](https://doc.dcloud.net.cn/uni-app-x/api/request.html) 的success原始回调结果，包括 `header`、`cookies`

## 注意事项
+ 该请求库目的是对 [uni.request](https://doc.dcloud.net.cn/uni-app-x/api/request.html) 的promise返回泛型封装，所以不具备拦截器功能。可以自己二次封装拦截器。
+ 传递泛型参数时一定要和服务器接口返回的数据结构对应，设计泛型类型时一定要兼容服务器接口返回的所有结构结果。
+ 该请求库设计初衷是为了摒弃传统的try catch写法。如果不习惯，可以切换回传统的写法，比如：

	```ts
	await request<Response>({
		url: 'https://test.api.fdproxy.cn/user/list'
	} as RequestOptions<Response>)
		.then((res) => {
			const { data, response } = res;
			console.log(response);
			if (data != null) {
				console.log(data.data);
			}
		})
		.catch((error) => {
			console.log(error);
		})
	```

---
### 结语
#### kux 不生产代码，只做代码的搬运工，致力于提供uts 的 js 生态轮子实现，欢迎各位大佬在插件市场搜索使用 kux 生态插件：[https://ext.dcloud.net.cn/search?q=kux](https://ext.dcloud.net.cn/search?q=kux)

### 友情推荐
+ [TMUI4.0](https://ext.dcloud.net.cn/plugin?id=16369)：包含了核心的uts插件基类.和uvue组件库
+ [GVIM即时通讯模版](https://ext.dcloud.net.cn/plugin?id=16419)：GVIM即时通讯模版，基于uni-app x开发的一款即时通讯模版
+ [t-uvue-ui](https://ext.dcloud.net.cn/plugin?id=15571)：T-UVUE-UI是基于UNI-APP X开发的前端UI框架
+ [UxFrame 低代码高性能UI框架](https://ext.dcloud.net.cn/plugin?id=16148)：【F2图表、双滑块slider、炫酷效果tabbar、拖拽排序、日历拖拽选择、签名...】UniAppX 高质量UI库
+ [wx-ui 基于uni-app x开发的高性能混合UI库](https://ext.dcloud.net.cn/plugin?id=15579)：基于uni-app x开发的高性能混合UI库，集成 uts api 和 uts component，提供了一套完整、高效且易于使用的UI组件和API，让您以更少的时间成本，轻松完成高性能应用开发。
+ [firstui-uvue](https://ext.dcloud.net.cn/plugin?id=16294)：FirstUI（unix）组件库，一款适配 uni-app x 的轻量、简洁、高效、全面的移动端组件库。
+ [easyXUI 不仅仅是UI 更是为UniApp X设计的电商模板库](https://ext.dcloud.net.cn/plugin?id=15602)：easyX 不仅仅是UI库，更是一个轻量、可定制的UniAPP X电商业务模板库，可作为官方组件库的补充,始终坚持简单好用、易上手